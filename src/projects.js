export default [
  { 
    id: 'kozea',
    name: '@Kozea group',
    baseline: 'Web services for pharmacies',
    description: 'I am maintaining and developping new features for an application that provides web services for pharmacies (ecommerce, blog, news, newsletters, chat, prescription reservation). I am also working on the internal platform used for billing and contracts management.',
    stack: 'Flask (Restful API), PostgreSQL, Sqlite3, ReactJS (SSR, PWA), Redux, uWSGI, NGINX, Gitlab CI'
  },
  { 
    id: 'melomnia',
    name: 'Melomnia',
    baseline: 'Music education network',
    description: 'It intends to become a platform for all music education actors. I am currently restructuring code, style and experimenting some tools like Django Rest Framework and Docker.',
    stack: 'Django, PostgreSQL, Javascript, Materialize, Gunicorn, NGINX, Travis CI, Fabric',
    url: 'https://www.melomnia.fr',
  },
  { 
    id: 'purbeurre',
    name: 'Pur Beurre',
    baseline: 'Find healthier products',
    description: 'This project has been made during my Openclassrooms course. It uses data from OpenFoodFact API to find healthier product.',
    stack: 'Django, PostgreSQL, JQuery, Bootstrap, Heroku',
    url: 'http://eatbetter2019.herokuapp.com/',
    repo_url: 'https://github.com/cmigazzi/P10_Pur_Beurre'
  },
  { 
    id: 'grandpy',
    name: 'Grandpy Bot',
    baseline: 'Chatbot for searching places and learn history',
    description: 'This project has been made during my Openclassrooms course. It uses Google Geocoding API to find places and Wikipedia API to provide history about it.',
    stack: 'Flask, Google Geocoding API, Heroku',
    repo_url: 'https://github.com/cmigazzi/P7_GrandpyBot'
  },
]