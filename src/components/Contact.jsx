import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import Github from '../assets/github.svg'
import Gitlab from '../assets/gitlab.svg'
import Linkedin from '../assets/linkedin.svg'

function Contact() {
  const [className, setClassName] = useState('contact in')

  useEffect(() => { 
    setClassName('contact')
  }, [])

  return (
    <section className={className}>
      <a href="https://www.github.com/cmigazzi/" target="_blank" rel="noopener noreferrer">
        <Github />
      </a>
      <a href="https://www.gitlab.com/cmigazzi/" target="_blank" rel="noopener noreferrer">
        <Gitlab />
      </a>
      <a href="https://www.linkedin.com/in/c%C3%A9dric-migazzi-b24545174/" target="_blank" rel="noopener noreferrer">
        <Linkedin />
      </a>
    </section>
  )
}

export default Contact