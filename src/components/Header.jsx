import React from 'react'
import { Link } from 'react-router-dom'

function Header ({ onNavClick, activePage }) {
  return (
    <header>
      <nav>
        <ul>
          <Link to="/">
            <li>
              About
            </li>
          </Link>
          <Link to="/projects">
            <li>
              Projects
            </li>
          </Link>
          <Link to="/contact">
            <li>
              Contact
            </li>
          </Link>
        </ul>
      </nav>
    </header>
  )
}

export default Header
