import React, { useState, useEffect } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'

import projects from '../projects'
import Times from '../assets/times.svg'

function Project() {
  const navigate = useNavigate()
  const [className, setClassName] = useState('project project__focus project__focus--in') 
  const { id: projectId } = useParams()
  const project = projectId && projects.find(p => p.id === projectId)

  useEffect(() => {
    setClassName('project project__focus')
  }, [])

  const handleClose = (e) => {
    e.preventDefault()
    navigate('/projects')

  }
  return (
      <section className={className}>
        <Times className="close" onClick={handleClose}/>
        <h3 className="project__title">{project.name}</h3>
        <p className="project__baseline">{project.baseline}</p>
        <hr/>
        <article className="project__content">
          <h4>Description</h4>
          <p>{project.description}</p>
        </article>
        <hr/>
        <article className="project__content">
          <h4>Stack</h4>
          <p>{project.stack}</p>
        </article>
        {(project.url || project.repo_url) &&
          <>
          <hr/>
          <article className="project__links">
            {project.repo_url && <p><a className="project__link" href={project.repo_url} target="_blank" rel="noopener noreferrer">View code</a></p>}
            {project.url && <p><a className="project__link" href={project.url} target="_blank" rel="noopener noreferrer">Visit...</a></p>}
          </article>
          </>
        }
      </section>
  )
}

export default Project
