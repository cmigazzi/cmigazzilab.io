import React, { useState, useEffect } from 'react'

function About() {
  const [className, setClassName] = useState('about in')

  useEffect(() => { 
    setClassName('about')
  }, [])

  return (
    <section className={className}>
      <h1>Hi,</h1>
      <p>My name is Cédric and I am a fullstack developer at La Belle Vie.</p>
      <p>I am currently working with Django, DRF, ReactJS, VueJS and a bit of Golang</p>
      <h1>As a developer...</h1>
      <p>
        Python is my favorite language. It permits to develop quite everything
        very quickly. I have worked  CLI program click, web projects, financial 
        data scraping.

        My favourite stack turns around Python (Flask/Django), React and PostgreSQL.
        <br/>
        I use to work in a Linux environment and I am familiar with tools
        like Gitlab CI, Ansible, Docker, Nginx, Gunicorn and uWSGI.
      </p>
      <h1>Before that...</h1>
      <p>
        Before becoming a professionnal developer, I was musician and teacher graduate
        with a Master In Music Pedagogy from HEMU of Lausanne (CH) and
        with a Bachelor in saxophone jazz from Brussels Royal Conservatory (B).
      </p>
    </section>
  )
}

export default About