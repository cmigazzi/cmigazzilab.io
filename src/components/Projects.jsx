import React, { useState, useEffect } from 'react'
import { Link, useParams, useNavigate, useLocation } from 'react-router-dom'

import projects from '../projects'

function Projects() {
  const [className, setClassName] = useState('projects in')
  const navigate = useNavigate()

  useEffect(() => { 
    setClassName('projects')
  }, [])

  const handleProjectClick = (id) => {
    navigate(`/projects/${id}`)
  }

  return (
      <section className={className}>
       {projects.map(p =>
        <article
          key={p.id}
          className={`project project__mini ${p.id}`}
          onClick={() => handleProjectClick(p.id)}
        >
          <h3>{p.name}</h3>
          <p>{p.baseline}</p>
        </article>
       )}
      </section>
  )
}

export default Projects
