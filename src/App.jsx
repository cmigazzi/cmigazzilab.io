import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import About from './components/About'
import Header from './components/Header'
import Contact from './components/Contact'
import Project from './components/Project'
import Projects from './components/Projects'

import './App.sass'

function App() {

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exact path="/" element={<About />} />
        <Route path="/projects/:id" element={<Project />} />
        <Route strict exact path="/projects" element={<Projects />} />
        <Route path="/contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
